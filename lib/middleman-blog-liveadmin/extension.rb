# Require core library
require 'middleman-core'

# Extension namespace
class Middleman::BlogLiveAdmin < ::Middleman::Extension
  option :layout, 'blog-liveadmin', 'Admin-specific layout'

  def initialize(app, options_hash={}, &block)
    # Call super to build options from the options_hash
    super

    raise "Blog Live Admin is not meant to be activated for build" if app.build?

    # Require libraries only when activated
    #require 'pathname'

    @assets_path = File.join(File.expand_path('../../..', __FILE__), 'assets')

    # catch preview server callback
    app.before_server(&method(:before_server))

    logger.info "== Blog Live Admin activated"
  end

  def after_configuration
    # easier than looping on files and creating ::Middleman::Sitemap::Resource objects ourselves
    app.config_context.import_path(File.join(@assets_path, "*")) do |path|
      # remove leading double-dots from the glob and keep the final extension only
      link = path.gsub(/^\.\./, '').gsub(/^(.*\/[^.]+\.[^.]+).*$/, '\1')
      # does some magic invisible to puts but necessary
      ::Middleman::Util.normalize_path(link)
    end

    # SASS search path needs is only set to :css_path
    ::Sass.load_paths.concat([File.join(@assets_path, "stylesheets")])

    # watch assets
    # (without it, many calls like app.file.find() fails and nothing works)
    app.files.watch :source, path: @assets_path

    # our layout
    app.config_context.page "/admin/*", :layout => options.layout

    # Sprockets integration
    if app.config_context.respond_to? :sprockets
      app.config_context.sprockets.append_path(@assets_path)
      app.config_context.sprockets.append_path(File.join(@assets_path, "stylesheets"))
      app.config_context.sprockets.append_path(File.join(@assets_path, "javascripts"))
    end
  end

  def before_server(server_information)
    logger.info "== Blog Live Admin interface available at #{server_information.site_addresses.first}/admin"
  end

  # helpers do
  #   def a_helper
  #   end
  # end
end
