require "middleman-core"

Middleman::Extensions.register :blog_liveadmin do
  require_relative "middleman-blog-liveadmin/extension"
  Middleman::BlogLiveAdmin
end
